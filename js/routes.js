angular.module('app.routes', ['ngRoute'])

.config(function($routeProvider, $locationProvider) {
  $routeProvider
        .when('/', {
		    templateUrl: 'vistas/sistema.html',
		    controller: 'sistemaCtrl'
        })

		.otherwise({
		  	redirectTo: '/'
		});

		$locationProvider.hashPrefix('');
    	$locationProvider.html5Mode(true);

});
