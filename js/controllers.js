angular.module('app.controllers', ['ngMaterial', 'ngMessages'])
.controller('loginCtrl', function($scope, restServices,  $timeout){

	$scope.user="";
	$scope.pass="";

$scope.openRegistro=function(){
	$('.container').stop().addClass('active');
}

$scope.closeRegistro=function(){
 	$('.container').stop().removeClass('active');
}

$('#user').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$('#pass').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$scope.loadingLogin = false;


			$scope.login = function() {

	
					if ($scope.pass != "" && $scope.user != "" && $scope.pass != undefined && $scope.user != undefined){
				
					$scope.loadingLogin = true;
					$timeout(logueo, 1500);

					} 
					
				
			}

			function logueo() {
    		
				restServices.Login($scope.user,$scope.pass).success(function(response) {
		                if(response.estado==1){

		                		alert("Inicio Exitoso");
		                		$('#botonLogin').blur();

		                } else if(response.estado==2) {
								
								alert("Datos Incorrectos");
								$('#botonLogin').blur();

		                }

							$scope.loadingLogin = false;
            	}).error(function () {
                      	alert("Error");
                      
                })

			}



		



  })
.controller('sistemaCtrl', function($scope, restServices,  $timeout){

			$scope.alumno = {
			"nombre":"",
			"apellido":"",
			"curso":""
			};


		$scope.registrar = function() {

	
					if ($scope.alumno.nombre != "" && $scope.alumno.nombre != "" && $scope.alumno.apellido != undefined && $scope.alumno.apellido != undefined && $scope.alumno.curso != undefined && $scope.alumno.curso != undefined){

					regis();

					} else {

					alert("Faltan Datos");

					}
					
				
			}

			function regis() {

				restServices.registrarAlumno($scope.alumno).success(function(response) {
		                if(response.estado==1){

		                		alert("Registro Exitoso");

		                		$scope.todosUser();


		                } else if(response.estado==2) {
								
								alert("Registro Error");
		                }
            	}).error(function () {
                      	alert("Error");
                      
                })

			}

		$scope.totalUsers = "";
		$scope.totalCursos = "";

			

			$scope.todosUser = function(){
				
				restServices.alumnosTodos().success(function(response) {
		                if(response.estado==1){

		                		$scope.totalUsers = response.usuarios;


		                } else if(response.estado==2) {
								
								alert("Error al recuperar total de usuarios Registrados");

		                }

							$scope.loadingRegister = false;
            	}).error(function () {
                      	alert("Error");
                      
                })
			};

			

			$scope.todosCurso = function(){
				
				restServices.cursosTodos().success(function(response) {
		                if(response.estado==1){

		                		$scope.totalCursos = response.usuarios;


		                } else if(response.estado==2) {
								
								alert("Error al recuperar total de cursos Registrados");

		                }

							$scope.loadingRegister = false;
            	}).error(function () {
                      	alert("Error");
                      
                })
			};

			$scope.todosUser();
			$scope.todosCurso();

  })