angular.module('app.services', [])
.service('restServices', ['$http', function($http){

    var apiUrl = 'http://localhost/andresPrueba/restApi/';


    ///////////LOCALES


    this.registrarAlumno = function(alumno){
        return $http({
            method: 'POST',
            url: apiUrl+'/insert_alumno.php',
            data: alumno});
    };


    this.cursosTodos = function (){
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerCursosTodos.php'});
    };

    this.alumnosTodos = function () {
        return $http({
            method: 'GET',
            url: apiUrl+'/ObtenerUsuariosTodos.php'});

    };
}]);
