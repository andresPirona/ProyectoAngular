<?php

/**
 * Representa el la estructura de las metas
 * almacenadas en la base de datos
 */
require 'Database.php';

class Alumnos
{
    function __construct()
    {
    }

    /**
     * Retorna en la fila especificada de la tabla 'meta'
     *
     * @param $idMeta Identificador del registro
     * @return array Datos del registro
     */

/////////////////////////////////PRINCIPALES





    /////////////////////////////GET FUNCTIONS

    public static function getAllCursos()
    {
        $consulta = "SELECT * FROM tblprofesores";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

           return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }
    
    public static function getAllUsuarios()
    {
        $consulta = "SELECT * FROM tblalumnos";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

           return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }
     
     

    /////////////////////////////////////////INSERT FUNCTIONS

    public static function insertNewAlumno(
        $nombre,
        $apellido,
        $curso
    )
    {
        // Sentencia INSERT
        $comando = "INSERT INTO tblalumnos ( " .
            "nombre," .
            " apellido," .
            " curso)" .
            " VALUES(?,?,?)";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(
            array(
                    $nombre,
                    $apellido,
                    $curso
            )
        );

    }


}

?>